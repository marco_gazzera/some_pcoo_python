'''
Constuction d'un projet de personne, de leur moyen de transport, leur lieu d'habitation et leur origine
'''
def Espace():
	print("\n")

class Personne:
	# -- Constructeur de personne -- #
	def __init__(self, nom: str = " ", age: int = 0, genre: bool = True):   
		self.nom = nom 
		self.age = age
		self.genre = genre

	#-- Methodes et code sur les personnes --#
	def SePressenter(self): 
		if self.genre :
			if self.age == 0:
				print("Bonjour Monsieur " + self.nom)
			else:
				print("Bonjour Monsieur " + self.nom + ", qui a " + str(self.age) + " ans")
				if (self.EstMajeur()):
					print("Vous etes majeur et donc tu peux avoir un véhicule")
				else:
					print("Vous etes mineur et donc tu ne peux pas conduire ton véhicule")
		else:
			if self.age == 0:
				print("Bonjour Madame " + self.nom)
			else:
				print("Bonjour Madame " + self.nom + ", qui a " + str(self.age) + " ans")
				if (self.EstMajeur()):
					print("Vous etes majeur et donc tu peux avoir un véhicule")
				else:
					print("Vous etes mineur et donc tu ne peux pas conduire ton véhicule")

	def EstMajeur(self):
		return self.age >= 18

class MoyenDeTransport:
	# -- Constructeur de moyen de transport -- #
	def __init__(self, nom: str = " ", voiture: bool = True, annee: int = 0, puissance: int = 0):  
		self.nom = nom 
		self.voiture = voiture 
		self.annee = annee
		self.puissance = puissance
		if nom == " ":
			self.DemanderNom()
		print("Moyen de transport: " + self.nom )

	#-- Methodes et code sur les moyens de transports --#
	def SePressenterMoyenTransport(self): 
		if self.voiture :
			if self.annee == 2022:
				print("Voiture toute neuve " + self.nom)
			else:
				print("Voiture " + self.nom + ", qui a " + str(self.annee) + " ans")
				if (self.EstHistorique()):
					print("Voiture est historique")
				else:
					print("Voiture n'est pas historique")
			if (self.Assurance()):
				print("L'assurance coute beaucoup")
			else:
				print("L'assurance ne coute pas beaucoup")
		else:
			if self.annee == 2022:
				print("Moto toute neuve " + self.nom)
			else:
				print("Moto " + self.nom + ", qui est de " + str(self.annee))
				if (self.EstHistorique()):
					print("Je suis historique")
				else:
					print("Je ne suis pas historique")
			if (self.Assurance()):
				print("L'assurance coute beaucoup")
			else:
				print("L'assurance ne coute pas beaucoup")

	def EstHistorique(self):
		return self.annee <= 1992
		
	def Assurance(self):
		return self.puissance >= 200

	def DemanderNom(self):
		self.nom = input("Nom du moyen de transport ")

class LieuxHabitation:
	# -- Constructeur de moyen de lieux habitation -- #
	def __init__(self, nom: str = " ", rue: bool = True, boulevard: bool = True):  
		self.nom = nom 
		self.rue = rue 
		self.boulevard = boulevard
		if nom == " ":
			self.DemanderNom()
		print("Nom du lieu: " + self.nom )
	#-- Methodes et code sur les moyens de transports --#
	def SePressenterLieux(self): 
		if self.rue:
			print("L adresse comporte le mot rue")
		else:
			print("L adresse ne comporte pas le mot rue")
		if self.boulevard:
			print("L adresse comporte le mot boulevard")
		else:
			print("L adresse ne comporte pas le mot boulevard")

class Origine:
	# -- Constructeur de moyen de lieux habitation -- #
	def __init__(self, europe: bool = True):  
		self.europe = europe 

	def SePressenterOrigine(self): 
		if self.europe:
			print("La personne vient d europe")
		else:
			print("La personne ne vient pas d europe")
		

# -- Utilisation --#
# Personnes # # Moyens de transports# #Lieux habitation# #Origines #
personne1 = Personne("Marco", 21, True) 
personne1.SePressenter()
moyen1 = MoyenDeTransport("4L", True, 1991, 175) 
moyen1.SePressenterMoyenTransport()
lieux1 = LieuxHabitation("Hotel des poste", True, False)
lieux1.SePressenterLieux()
origine1 = Origine(True)
origine1.SePressenterOrigine()
Espace()
personne2 = Personne("Margot", 15, False)
personne2.SePressenter()
moyen2 = MoyenDeTransport("Fiat 500", True, 2022, 75) 
moyen2.SePressenterMoyenTransport()
lieux2 = LieuxHabitation("Mont Boron", False, True)
lieux2.SePressenterLieux()
origine2 = Origine(True)
origine2.SePressenterOrigine()
Espace()
personne3 = Personne("Arkos", 22, True) 
personne3.SePressenter()
moyen3 = MoyenDeTransport(" ", True, 2013, 175) 
moyen3.SePressenterMoyenTransport()
lieux3 = LieuxHabitation("Jean Medecin", False, False)
lieux3.SePressenterLieux()
origine3 = Origine(True)
origine3.SePressenterOrigine()
Espace()
personne4 = Personne("Arkossette", 12, False)
personne4.SePressenter()
moyen4 = MoyenDeTransport("V-Rapto", True, 2022, 175) 
moyen4.SePressenterMoyenTransport()
lieux4 = LieuxHabitation("De France", True, False)
lieux4.SePressenterLieux()
origine4 = Origine(False)
origine4.SePressenterOrigine()
Espace()
personne5 = Personne("Pier", 8, True) 
personne5.SePressenter()
moyen5 = MoyenDeTransport("FTU-M 700", True, 1980, 75) 
moyen5.SePressenterMoyenTransport()
lieux5 = LieuxHabitation("De la madelaine", False, True)
lieux5.SePressenterLieux()
origine5 = Origine(False)
origine5.SePressenterOrigine()
Espace()
personne6 = Personne("Pierrette", 56, False)
personne6.SePressenter()
moyen6 = MoyenDeTransport(" ", True, 2013, 175) 
moyen6.SePressenterMoyenTransport()
lieux6 = LieuxHabitation("De la californie", False, False)
lieux6.SePressenterLieux()
origine6 = Origine(False)
origine6.SePressenterOrigine()
Espace()